FROM node:8.11

ENV web /opt/web

WORKDIR $web
COPY ./web/package.json ./web/package-lock.json $web/
RUN npm install

COPY ./web $web

CMD ["npm", "run", "develop", "--", "--host=0.0.0.0"]
